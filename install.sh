#!/usr/bin/env sh

# clean
rm -rf ${HOME}/.vim{,rc}

# symbolic links
ln -s ${PWD} ${HOME}/.vim
ln -s ${PWD}/vimrc ${HOME}/.vimrc

# pathogen
mkdir -p ${HOME}/.vim/autoload ${HOME}/.vim/bundle && \
  curl -LSso ${HOME}/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
